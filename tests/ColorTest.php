<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Tests;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use BoomDraw\ColorConverter\Color;
use RuntimeException;
use Throwable;

class ColorTest extends TestCase
{
    protected $wrongRgb = [300, 300, 300];
    protected $rgba = [200, 200, 200, 0];
    protected $rgbWithBgRGB = [255, 255, 255];
    protected $alpha = 1;
    protected $hexToRgb = [[245, 255, 250], [255, 255, 255], [255, 255, 255], [255, 255, 255]];
    protected $cmyk = [
        [0.32015810277, 0, 0.13833992095, 0.00784313725],
        [0.80, 0.60, 0.0, 0.98039215686],
        [0.0, 0.0, 0.0, 0.67843137255]
    ];
    protected $RGBs = [[172, 253, 218], [1, 2, 5], [82, 82, 82]];
    protected $xyz = [
        [64.79342620938, 84.08317098225, 79.14436863222],
        [0.0616190129, 0.06082680750, 0.1520730893],
        [8.019958907271, 8.437621154415, 9.188569437158],
    ];
    protected $cmy = [
        [0.32549019608, 0.00784313725, 0.14509803922],
        [0.996078431372549, 0.9921568627450981, 0.9803921568627451],
        [0.6784313725490196, 0.6784313725490196, 0.6784313725490196]
    ];
    protected $lab = [
        [93.48661186259, -31.87624373435, 8.94505900115],
        [0.54944368603, 0.15586605436, -1.22784957266],
        [34.87815216307667, 0.0023072803218282534, -0.004565079276031003]
    ];
    protected $lch = [
        [93.4866, 33.1075, 164],
        [0.5494, 1.2377, 277],
        [34.8782, 0.0051, 296]
    ];
    protected $yxy = [
        [84.08317098224839, 0.2841555642713679, 0.3687519289222016],
        [0.06082680750318704, 0.2244618157292438, 0.22157602025585174],
        [8.437621154414881, 0.3127159072215825, 0.3290014805066623],
    ];
    protected $hlab = [
        [91.69687616394, -29.91092602165, 8.33123891866],
        [2.46630913519, 0.27976169912, -2.14300428786],
        [29.04758364204, 0.00158021892, -0.00304035582],
    ];
    protected $luv = [
        [93.48661186259, -38.97793039694, 19.05754438659],
        [0.54944368603, -0.18219715075, -0.61123941283],
        [34.87815216308, 0.00031064910, -0.00596693784],
    ];
    protected $hsb = [
        [154, 0.32015810277, 0.99215686275],
        [225, 0.8, 0.01960784314],
        [0, 0, 0.32156862745],
    ];
    protected $rgbArray = [[245, 255, 250], [175, 238, 238]];
    protected $HSL = [[150, 1.0, 0.98039215686], [180, 0.64948453608, 0.80980392157]];
    protected $HSLA = [[150, 1.0, 0.98039215686, 1], [180, 0.64948453608, 0.80980392157, 1]];
    protected $safe = [[255, 255, 255], [153, 255, 255]];
    protected $hex = ['#f5fffa', '#f', '#ff', '#fff'];
    protected $wrongHex = ['1234567', '1234', '12345'];
    protected $names = ['mintcream', 'paleturquoise'];


    public function testSetGetRGB(): void
    {
        $color = new Color();
        foreach ($this->rgbArray as $rgb) {
            $color->setRGB(...$rgb);
            self::assertEquals($this->prepareValues($color->getRGB()), $rgb);
        }
    }

    public function testCheckColorException(): void
    {
        $color = new Color();
        $this->expectException(InvalidArgumentException::class);
        $color->setRGB(...$this->wrongRgb);
    }

    public function testCheckBGColorException(): void
    {
        $color = new Color();
        $this->expectException(InvalidArgumentException::class);
        $color->setBgRGB(...$this->wrongRgb);
    }

    public function testGetBgRGB(): void
    {
        $color = new Color();
        self::assertEquals($this->prepareValues($color->getBgRGB()), $this->rgbWithBgRGB);
    }

    public function testGetRGBValue(): void
    {
        $color = new Color();
        foreach ($this->rgbArray as $rgb) {
            $color->setRGB(...$rgb);
            self::assertEquals($color->getRGB('r'), $rgb[0]);
            self::assertEquals($color->getRGB('red'), $rgb[0]);
            self::assertEquals($color->getRGB('green'), $rgb[1]);
            self::assertEquals($color->getRGB('g'), $rgb[1]);
            self::assertEquals($color->getRGB('blue'), $rgb[2]);
            self::assertEquals($color->getRGB('b'), $rgb[2]);
        }
    }

    public function testSetGetRgbWithAlpha(): void
    {
        $color = new Color();
        foreach ($this->rgbArray as $rgb) {
            $color->setRGB(...$rgb);
            $color->setAlpha(0);
            self::assertEquals($this->prepareValues($color->getRGB()), $this->rgbWithBgRGB);
        }
    }

    public function testSetGetAlpha(): void
    {
        $color = new Color();
        $color->setAlpha($this->alpha);
        self::assertEquals($color->getAlpha(), $this->alpha);
    }

    public function testSetAlphaException(): void
    {
        $alpha = 2;
        $color = new Color();
        $this->expectException(InvalidArgumentException::class);
        $color->setAlpha($alpha);
    }

    public function testSetRGBA(): void
    {
        $color = new Color();
        $color->setRGBA(200, 200, 200, 0, 100, 100, 100);
        self::assertEquals($this->prepareValues($color->getRGBA()), $this->rgba);
    }

    public function testSetHEX(): void
    {
        $color = new Color();
        foreach ($this->hex as $index => $hexCode) {
            $color->setHEX($hexCode);
            self::assertEquals($this->prepareValues($color->getRGB()), $this->hexToRgb[$index]);
        }
    }

    public function testSetHEXException(): void
    {
        $color = new Color();
        foreach ($this->wrongHex as $hex) {
            try {
                $color->setHEX($hex);
            } catch (Throwable $exception) {
                self::assertInstanceOf(InvalidArgumentException::class, $exception);
            }
        }
    }

    public function testGetHEX(): void
    {
        $color = new Color();
        $color->setHEX($this->hex[0]);
        $hex = trim($this->hex[0], '#');
        self::assertEquals($color->getHEX(), $hex);
    }

    public function testSetCMYK(): void
    {
        $color = new Color();
        foreach ($this->cmyk as $key => $cmyk) {
            $color->setCMYK(...$cmyk);
            self::assertEquals($this->prepareValues($color->getRGB()), $this->RGBs[$key]);
        }
    }

    public function testGetCMYK(): void
    {
        $color = new Color();
        foreach ($this->RGBs as $key => $rgb) {
            $color->setRGB(...$rgb);
            self::assertEquals($this->prepareValues($color->getCMYK()), $this->cmyk[$key]);
        }
    }

    public function testSetCMY(): void
    {
        $color = new Color();
        foreach ($this->cmy as $key => $cmy) {
            $color->setCMY(...$cmy);
            self::assertEquals($this->prepareValues($color->getRGB()), $this->RGBs[$key]);
        }
    }

    public function testGetCMY(): void
    {
        $color = new Color();
        foreach ($this->RGBs as $key => $rgb) {
            $color->setRGB(...$rgb);
            self::assertEquals($this->prepareValues($color->getCMY()), $this->cmy[$key]);
        }
    }

    public function testSetLab(): void
    {
        $color = new Color();
        foreach ($this->lab as $key => $lab) {
            $color->setLab(...$lab);
            self::assertEquals($this->prepareValues($color->getRGB()), $this->RGBs[$key]);
        }
    }

    public function testGetLab(): void
    {
        $color = new Color();
        foreach ($this->RGBs as $key => $rgb) {
            $color->setRGB(...$rgb);
            self::assertEquals($this->prepareValues($color->getLab()), $this->lab[$key]);
        }
    }

    public function testSetLCH(): void
    {
        $color = new Color();
        foreach ($this->lch as $key => $lch) {
            $color->setLCH(...$lch);
            self::assertEquals($this->prepareValues($color->getRGB()), $this->RGBs[$key]);
        }
    }

    public function testGetLCH(): void
    {
        $color = new Color();
        foreach ($this->RGBs as $key => $rgb) {
            $color->setRGB(...$rgb);
            self::assertEquals($this->prepareValues($color->getLCH()), $this->lch[$key]);
        }
    }

    public function testSetHunterLab(): void
    {
        $color = new Color();
        foreach ($this->hlab as $key => $lab) {
            $color->setHunterLab(...$lab);
            self::assertEquals($this->prepareValues($color->getRGB()), $this->RGBs[$key]);
        }
    }

    public function testGetHunterLab(): void
    {
        $color = new Color();
        foreach ($this->RGBs as $key => $rgb) {
            $color->setRGB(...$rgb);
            self::assertEquals($this->prepareValues($color->getHunterLab()), $this->hlab[$key]);
        }
    }

    public function testSetLuv(): void
    {
        $color = new Color();
        foreach ($this->luv as $key => $luv) {
            $color->setLuv(...$luv);
            self::assertEquals($this->prepareValues($color->getRGB()), $this->RGBs[$key]);
        }
    }

    public function testGetLuv(): void
    {
        $color = new Color();
        foreach ($this->RGBs as $key => $rgb) {
            $color->setRGB(...$rgb);
            self::assertEquals($this->prepareValues($color->getLuv()), $this->luv[$key]);
        }
    }

    public function testSetHSV(): void
    {
        $color = new Color();
        foreach ($this->hsb as $key => $hsb) {
            $color->setHSV(...$hsb);
            self::assertEquals($this->prepareValues($color->getRGB()), $this->RGBs[$key]);
        }
    }

    public function testGetHSV(): void
    {
        $color = new Color();
        foreach ($this->RGBs as $key => $rgb) {
            $color->setRGB(...$rgb);
            self::assertEquals($this->prepareValues($color->getHSV()), $this->hsb[$key]);
        }
    }

    public function testSetXYZ(): void
    {
        $color = new Color();
        foreach ($this->xyz as $key => $xyz) {
            $color->setXYZ(...$xyz);
            self::assertEquals($this->prepareValues($color->getRGB()), $this->RGBs[$key]);
        }
    }

    public function testGetXYZ(): void
    {
        $color = new Color();
        foreach ($this->RGBs as $key => $rgb) {
            $color->setRGB(...$rgb);
            self::assertEquals($this->prepareValues($color->getXYZ()), $this->xyz[$key]);
        }
    }

    public function testSetYxy(): void
    {
        $color = new Color();
        foreach ($this->yxy as $key => $yxy) {
            $color->setYxy(...$yxy);
            self::assertEquals($this->prepareValues($color->getRGB()), $this->RGBs[$key]);
        }
    }

    public function testGetYxy(): void
    {
        $color = new Color();
        foreach ($this->RGBs as $key => $rgb) {
            $color->setRGB(...$rgb);
            self::assertEquals($this->prepareValues($color->getYxy()), $this->yxy[$key]);
        }
    }

    public function testSetName(): void
    {
        $color = new Color();
        foreach ($this->names as $key => $name) {
            $color->setName($name);
            self::assertSame($this->prepareValues($color->getRGB()), $this->rgbArray[$key]);
        }
    }

    public function testGetName(): void
    {
        $color = new Color();
        foreach ($this->rgbArray as $key => $rgb) {
            $color->setRGB(...$rgb);
            self::assertSame($color->getName(), $this->names[$key]);
        }
    }

    public function testSetHSL(): void
    {
        $color = new Color();
        foreach ($this->HSL as $key => $hsl) {
            $color->setHSL(...$hsl);
            self::assertEquals($this->prepareValues($color->getRGB()), $this->rgbArray[$key]);
        }
    }

    public function testGetHSL(): void
    {
        $color = new Color();
        foreach ($this->rgbArray as $key => $rgb) {
            $color->setRGB(...$rgb);
            self::assertEquals($this->prepareValues($color->getHSL()), $this->HSL[$key]);
        }
    }

    public function testSetHSLA(): void
    {
        $color = new Color();
        foreach ($this->HSLA as $key => $hsla) {
            $color->setHSLA(...$hsla);
            self::assertEquals($this->prepareValues($color->getRGB()), $this->rgbArray[$key]);
        }
    }

    public function testGetHSLA(): void
    {
        $color = new Color();
        foreach ($this->rgbArray as $key => $rgb) {
            $color->setRGB(...$rgb);
            self::assertEquals($this->prepareValues($color->getHSLA()), $this->HSLA[$key]);
        }
    }

    public function testSetHSI(): void
    {
        $color = new Color();
        foreach ($this->HSL as $key => $hsl) {
            $color->setHSI(...$hsl);
            self::assertEquals($this->prepareValues($color->getRGB()), $this->rgbArray[$key]);
        }
    }

    public function testGetHSI(): void
    {
        $color = new Color();
        foreach ($this->rgbArray as $key => $rgb) {
            $color->setRGB(...$rgb);
            self::assertEquals($this->prepareValues($color->getHSI()), $this->HSL[$key]);
        }
    }

    public function testSetHSIA(): void
    {
        $color = new Color();
        foreach ($this->HSLA as $key => $hsla) {
            $color->setHSIA(...$hsla);
            self::assertEquals($this->prepareValues($color->getRGB()), $this->rgbArray[$key]);
        }
    }

    public function testGetHSIA(): void
    {
        $color = new Color();
        foreach ($this->rgbArray as $key => $rgb) {
            $color->setRGB(...$rgb);
            self::assertEquals($this->prepareValues($color->getHSIA()), $this->HSLA[$key]);
        }
    }

    public function testSetGetSafe(): void
    {
        $color = new Color();
        foreach ($this->rgbArray as $key => $rgb) {
            $color->setRGB(...$rgb);
            $color->toSafe();
            self::assertEquals($this->prepareValues($color->getRGB()), $this->safe[$key]);
        }
    }

    public function testSetGetRandom(): void
    {
        $color = new Color();
        $color->setRandom();
        self::assertNotNull($this->prepareValues($color->getRGB()));
    }

    protected function prepareValues(array $arr): array
    {
        foreach ($arr as &$item) {
            if ($item == (int)$item) {
                $item = (int)$item;
            } else {
                $item = round($item, 11);
            }
        }

        return array_values($arr);
    }
}
