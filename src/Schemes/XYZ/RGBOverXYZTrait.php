<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes\XYZ;

trait RGBOverXYZTrait
{
    /**
     * Convert the RGB color to a scheme color.
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return array<float>
     */
    public function fromRGB(int $red, int $green, int $blue): array
    {
        ['x' => $x, 'y' => $y, 'z' => $z] = (new XYZScheme)->fromRGB($red, $green, $blue);

        return $this->fromXYZ($x, $y, $z);
    }

    /**
     * Convert the XYZ color to a scheme color.
     *
     * @param float $x
     * @param float $y
     * @param float $z
     * @return array<float>
     */
    abstract public function fromXYZ(float $x, float $y, float $z): array;

    /**
     * Convert the scheme color to a RGB color.
     *
     * @param float $l
     * @param float $a
     * @param float $b
     * @return array<int>
     */
    public function toRGB(float $l, float $a, float $b): array
    {
        ['x' => $x, 'y' => $y, 'z' => $z] = $this->toXYZ($l, $a, $b);

        return (new XYZScheme)->toRGB((float)$x, (float)$y, (float)$z);
    }

    /**
     * Convert the color to a XYZ color.
     *
     * @param float $l
     * @param float $a
     * @param float $b
     * @return array<float>
     */
    abstract public function toXYZ(float $l, float $a, float $b): array;
}
