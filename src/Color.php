<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter;

use Exception;
use InvalidArgumentException;

class Color
{
    /**
     * Number 0-255 for current RGB color red value.
     * @var int
     */
    protected $red = 255;

    /**
     * Number 0-255 for current RGB color green value.
     * @var int
     */
    protected $green = 255;

    /**
     * Number 0-255 for current RGB color blue value.
     * @var int
     */
    protected $blue = 255;

    /**
     * Current color alpha value.
     * @var float
     */
    protected $alpha = 1.0;

    /**
     * Number 0-255 for current color RGB background red value.
     * @var int
     */
    protected $bgRed = 255;

    /**
     * Number 0-255 for current color RGB background green value.
     * @var int
     */
    protected $bgGreen = 255;

    /**
     * Number 0-255 for current color RGB background blue value.
     * @var int
     */
    protected $bgBlue = 255;

    /**
     * @var Converter
     */
    protected $converter;

    /**
     * Color constructor.
     */
    public function __construct()
    {
        $this->converter = new Converter();
    }

    /**
     * @param string $name
     * @param array $attributes
     * @return mixed
     */
    public function __call(string $name, array $attributes)
    {
        $action = substr($name, 0, 3);
        $color = substr($name, 3);
        if ($action === 'set') {
            $action = "${$color}ToRGB";
            $rgb = $this->converter->$action(...$attributes);

            return $this->setRGB($rgb['red'], $rgb['green'], $rgb['blue']);
        }
        if ($action === 'get') {
            $rgb = $this->getRGB();
            $action = "RGBTo${$color}";

            return $this->converter->$action($rgb['red'], $rgb['green'], $rgb['blue']);
        }
    }

    /**
     * Sets the current color RGB value.
     *
     * @param int $red number 0-255 for blue color value
     * @param int $green number 0-255 for green color value
     * @param int $blue number 0-255 for blue color value
     *
     * @return $this
     */
    public function setRGB(int $red, int $green, int $blue): self
    {
        $this->red = $red;
        $this->green = $green;
        $this->blue = $blue;

        $this->resetAlpha();

        return $this->checkColor();
    }

    /**
     * Reset alpha channel value and background color values.
     *
     * @return $this
     */
    public function resetAlpha(): self
    {
        return $this->setAlpha(1.0)->setBgRGB(255, 255, 255);
    }

    /**
     * Set the current color background RGB value.
     *
     * @param int $red number 0-255 for blue color value
     * @param int $green number 0-255 for green color value
     * @param int $blue number 0-255 for blue color value
     *
     * @return $this
     */
    public function setBgRGB(int $red, int $green, int $blue): self
    {
        $this->bgRed = $red;
        $this->bgGreen = $green;
        $this->bgBlue = $blue;

        return $this->checkBgColor();
    }

    /**
     * Throw an exception if the background color values are not between 0 and 255.
     *
     * @return $this
     * @throws InvalidArgumentException
     */
    protected function checkBgColor(): self
    {
        if (!$this->isRGBValid($this->bgRed, $this->bgGreen, $this->bgBlue)) {
            throw new InvalidArgumentException('Invalid background color value!');
        }

        return $this;
    }

    /**
     * Determine the rgb colors are between 0 and 255.
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return bool
     */
    protected function isRGBValid(int $red, int $green, int $blue): bool
    {
        return $red >= 0 && $red <= 255
            && $green >= 0 && $green <= 255
            && $blue >= 0 && $blue <= 255;
    }

    /**
     * Throw an exception if the color values are not between 0 and 255.
     *
     * @return $this
     * @throws InvalidArgumentException
     */
    protected function checkColor(): self
    {
        if (!$this->isRGBValid($this->red, $this->green, $this->blue)) {
            throw new InvalidArgumentException('Invalid background color value!');
        }

        return $this;
    }

    /**
     * Returns the RGB value of a current color.
     *
     * @param string $val
     * @return array<int>|int
     */
    public function getRGB(string $val = '')
    {
        if ($this->alpha === 1.0) {
            $rgb = $this->compactColor($this->red, $this->green, $this->blue);
        } else {
            $rgb = $this->converter->RGBAToRGB(
                $this->red,
                $this->green,
                $this->blue,
                $this->alpha,
                $this->bgRed,
                $this->bgGreen,
                $this->bgBlue
            );
        }

        return $this->getRGBValue($rgb, $val);
    }

    /**
     * Return named array of the RGB values
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @param float|null $alpha
     * @return array<int|float>
     */
    protected function compactColor(int $red, int $green, int $blue, ?float $alpha = null): array
    {
        if (is_null($alpha)) {
            return compact('red', 'green', 'blue');
        }

        return compact('red', 'green', 'blue', 'alpha');
    }

    /**
     * Return the current color RGB background value.
     *
     * @param int[] $rgb
     * @param string $val
     * @return array<int>|int
     */
    protected function getRGBValue(array $rgb, string $val = '')
    {
        switch ($val) {
            case 'r':
            case 'red':
                return $rgb['red'];
            case 'g':
            case 'green':
                return $rgb['green'];
            case 'b':
            case 'blue':
                return $rgb['blue'];
            default:
                return $rgb;
        }
    }

    /**
     * Return the current color alpha value.
     *
     * @return float
     */
    public function getAlpha(): float
    {
        return $this->alpha;
    }

    /**
     * Sets the current color alpha value.
     *
     * @param float $alpha
     * @return $this
     */
    public function setAlpha(float $alpha): self
    {
        $this->alpha = $alpha;

        return $this->checkAlpha();
    }

    /**
     * Return the current color RGB background value.
     *
     * @param string $val
     * @return array<int>|int
     */
    public function getBgRGB(string $val = '')
    {
        $rgb = $this->compactColor($this->bgRed, $this->bgGreen, $this->bgBlue);

        return $this->getRGBValue($rgb, $val);
    }

    /**
     * Set the RGBA value.
     *
     * @param int $red number 0-255 for blue color value
     * @param int $green number 0-255 for green color value
     * @param int $blue number 0-255 for blue color value
     * @param float $alpha number 0-1 for alpha channel value
     * @param int $bgRed number 0-255 for alpha background blue color value
     * @param int $bgGreen number 0-255 for alpha background green color value
     * @param int $bgBlue number 0-255 for alpha background blue color value
     *
     * @return $this
     */
    public function setRGBA(
        int $red,
        int $green,
        int $blue,
        float $alpha,
        int $bgRed = 255,
        int $bgGreen = 255,
        int $bgBlue = 255
    ): self {
        $this->setRGB($red, $green, $blue)
            ->setAlpha($alpha)
            ->setBgRGB($bgRed, $bgGreen, $bgBlue);

        return $this;
    }

    /**
     * Set the HEX HTML color value
     *
     * @param string $hex 6,3,2, or 1 characters long.
     * @return $this
     */
    public function setHEX(string $hex): self
    {
        $hex = strtolower($hex);
        $hex = trim($hex, "# \t\n\r\0\x0B");
        switch (strlen($hex)) {
            case 1:
                $hex .= $hex . $hex . $hex . $hex . $hex;
                break;
            case 2:
                $hex = $hex[0] . $hex[1] . $hex[0] . $hex[1] . $hex[0] . $hex[1];
                break;
            case 3:
                $hex = $hex[0] . $hex[0] . $hex[1] . $hex[1] . $hex[2] . $hex[2];
                break;
            case 6:
                break;
            default:
                throw new InvalidArgumentException('Invalid HEX length!');
        }
        $rgb = $this->converter->HEXToRGB($hex);

        $this->setRGB($rgb['red'], $rgb['green'], $rgb['blue']);

        return $this;
    }

    /**
     * Set current color from HSLA schema.
     *
     * @param int $hue
     * @param float $saturation
     * @param float $lightness
     * @param float $alpha
     * @return $this
     */
    public function setHSLA(int $hue, float $saturation, float $lightness, float $alpha): self
    {
        $rgb = $this->converter->HSLToRGB($hue, $saturation, $lightness);

        return $this->setRGB($rgb['red'], $rgb['green'], $rgb['blue'])->setAlpha($alpha);
    }

    /**
     * Return current color HSLA value.
     *
     * @return array<int|float>
     */
    public function getHSLA(): array
    {
        $rgba = $this->getRGBA();

        return $this->converter->RGBAToHSLA($rgba['red'], $rgba['green'], $rgba['blue'], $rgba['alpha']);
    }

    /**
     * Return current color RGBA value.
     *
     * @return array<int|float>
     */
    public function getRGBA(): array
    {
        return $this->compactColor($this->red, $this->green, $this->blue, $this->alpha);
    }

    /**
     * Set current rgb color randomly.
     *
     * @return $this
     * @throws Exception
     */
    public function setRandom(): self
    {
        $rgb = $this->getRandom();

        return $this->setRGB($rgb['red'], $rgb['green'], $rgb['blue']);
    }

    /**
     * Return random rgb color.
     *
     * @return array<int>
     * @throws Exception
     */
    public function getRandom(): array
    {
        return $this->compactColor(random_int(0, 255), random_int(0, 255), random_int(0, 255));
    }

    /**
     * Throw an exception if alpha value is not between 0 and 1.
     *
     * @return $this
     * @throws InvalidArgumentException
     */
    protected function checkAlpha(): self
    {
        if ($this->alpha < 0 || $this->alpha > 1) {
            throw new InvalidArgumentException('Invalid alpha value!');
        }

        return $this;
    }
}
