<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes;

class LChScheme extends AbstractScheme
{
    /**
     * @var LabScheme
     */
    protected $labScheme;

    public function __construct()
    {
        $this->labScheme = new LabScheme();
    }

    /**
     * Convert the RGB color to a LCh color.
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return array<int|float>
     */
    public function fromRGB(int $red, int $green, int $blue): array
    {
        ['l' => $l, 'a' => $a, 'b' => $b] = $this->labScheme->fromRGB($red, $green, $blue);
        $h = atan2($b, $a);
        $h = $h > 0 ? ($h / M_PI) * 180 : 360 - (abs($h) / M_PI) * 180;

        $l = round($l, 4);
        $c = round(sqrt((($a ** 2) + ($b ** 2))), 4);
        $h %= 360;

        return compact('l', 'c', 'h');
    }

    /**
     * Convert the LCh color to a RGB color.
     *
     * @param float $l
     * @param float $c
     * @param float $h - range = 0 ÷ 360°
     * @return array<int>
     */
    public function toRGB(float $l, float $c, float $h): array
    {
        $radiansH = (M_PI * $h) / 180;
        $a = cos($radiansH) * $c;
        $b = sin($radiansH) * $c;

        return $this->labScheme->toRGB($l, $a, $b);
    }
}
