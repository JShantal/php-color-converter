<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Tests;

use BoomDraw\ColorConverter\Converter;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use RuntimeException;

class ConverterTest extends TestCase
{
    protected $colors = [
        [
            'name' => '',
            'rgba' => [172, 253, 218, 1],
            'rgb' => [172, 253, 218],
            'hex' => 'acfdda',
            'safe' => [153, 255, 204],
            'hsla' => [154, 0.95294117647, 0.83333333333, 1],
            'hlsa' => [154, 0.83333333333, 0.95294117647, 1],
            'hsl' => [154, 0.95294117647, 0.83333333333],
            'hsb' => [154, 0.32015810277, 0.99215686275],
            'cmyk' => [0.32015810277, 0, 0.13833992095, 0.00784313725],
            'xyz' => [64.79342620938, 84.08317098225, 79.14436863222],
            'cmy' => [0.32549019608, 0.00784313725, 0.14509803922],
            'lab' => [93.48661186259, -31.87624373435, 8.94505900115],
            'lch' => [93.4866, 33.1075, 164],
            'yxy' => [84.08317098225, 0.28415556427, 0.36875192892],
            'hlab' => [91.69687616394, -29.91092602165, 8.33123891866],
            'luv' => [93.48661186259, -38.97793039694, 19.05754438659],
        ],
        [
            'name' => '',
            'rgba' => [1, 2, 5, 1],
            'rgb' => [1, 2, 5],
            'hex' => '010205',
            'safe' => [0, 0, 0],
            'hsla' => [225, 0.66666666667, 0.01176470588, 1],
            'hlsa' => [225, 0.01176470588, 0.66666666667, 1],
            'hsl' => [225, 0.66666666667, 0.01176470588],
            'hsb' => [225, 0.8, 0.01960784314],
            'cmyk' => [0.80, 0.60, 0.0, 0.98039215686],
            'xyz' => [0.0616190129, 0.06082680750, 0.1520730893],
            'cmy' => [0.99607843137, 0.99215686275, 0.98039215686],
            'lab' => [0.54944368603, 0.15586605436, -1.22784957266],
            'lch' => [0.5494, 1.2377, 277],
            'yxy' => [0.06082680750, 0.22446181573, 0.22157602026],
            'hlab' => [2.46630913519, 0.27976169912, -2.14300428786],
            'luv' => [0.54944368603, -0.18219715075, -0.61123941283],
        ],
        [
            'name' => '',
            'rgba' => [82, 82, 82, 1],
            'rgb' => [82, 82, 82],
            'hex' => '525252',
            'safe' => [102, 102, 102],
            'hsla' => [0, 0, 0.32156862745, 1],
            'hlsa' => [0, 0.32156862745, 0, 1],
            'hsl' => [0, 0, 0.32156862745],
            'hsb' => [0, 0, 0.32156862745],
            'cmyk' => [0.0, 0.0, 0.0, 0.67843137255],
            'xyz' => [8.01995890727, 8.43762115442, 9.18856943716],
            'cmy' => [0.67843137255, 0.67843137255, 0.67843137255],
            'lab' => [34.87815216308, 0.00230728032, -0.00456507928],
            'lch' => [34.8782, 0.0051, 296],
            'yxy' => [8.43762115441, 0.31271590722, 0.32900148051],
            'hlab' => [29.04758364204, 0.00158021892, -0.00304035582],
            'luv' => [34.87815216308, 0.00031064910, -0.00596693784],
        ],
        [
            'name' => '',
            'rgba' => [237, 175, 175, 1],
            'rgb' => [237, 175, 175],
            'hex' => 'edafaf',
            'safe' => [255, 153, 153],
            'hsla' => [0, 0.63265306122, 0.80784313725, 1],
            'hlsa' => [0, 0.80784313725, 0.63265306122, 1],
            'hsl' => [0, 0.63265306122, 0.80784313725],
            'hsb' => [360, 0.26160337553, 0.92941176471],
            'zeroHsb' => [0, 0.26160337553, 0.92941176471],
            'cmyk' => [0, 0.26160337553, 0.26160337553, 0.07058823529],
            'xyz' => [57.99288769026, 51.75961460528, 47.49148775960],
            'cmy' => [0.07058823529, 0.31372549020, 0.31372549020],
            'lab' => [77.13687610671, 22.628911344370508, 8.90539330898],
            'lch' => [77.1369, 24.3182, 21],
            'yxy' => [51.75961460528, 0.36880829385, 0.32916752231],
            'hlab' => [71.94415515195, 22.17284664409, 7.58742530037],
            'luv' => [77.13687610671, 39.73645666273, 8.55798478612],
        ],
        [
            'name' => '',
            'rgba' => [36, 36, 24, 1],
            'rgb' => [36, 36, 24],
            'hex' => '242418',
            'safe' => [51, 51, 0],
            'hsla' => [60, 0.2, 0.11764705882, 1],
            'hlsa' => [60, 0.11764705882, 0.2, 1],
            'hsl' => [60, 0.2, 0.11764705882],
            'hsb' => [60, 0.33333333333, 0.14117647059],
            'cmyk' => [0.0, 0.0, 0.33333333333, 0.85882352941],
            'xyz' => [1.52330025518, 1.70276844126, 1.11253334931],
            'cmy' => [0.85882352941, 0.85882352941, 0.90588235294],
            'lab' => [13.84304863685, -2.57139152610, 8.05440624875],
            'lch' => [13.8430, 8.4549, 107],
            'yxy' => [1.70276844126, 0.35110393604, 0.39246937684],
            'hlab' => [13.04901697931, -1.32198336517, 3.49859894744],
            'luv' => [13.84304863685, 0.46401138895, 6.43041363667],
        ],
        [
            'name' => '',
            'rgba' => [24, 24, 36, 1],
            'rgb' => [24, 24, 36],
            'hex' => '181824',
            'safe' => [0, 0, 51],
            'hsla' => [240, 0.2, 0.11764705882, 1],
            'hlsa' => [240, 0.11764705882, 0.2, 1],
            'hsl' => [240, 0.2, 0.11764705882],
            'hsb' => [240, 0.33333333333, 0.14117647059],
            'cmyk' => [0.33333333333, 0.33333333333, 0.0, 0.85882352941],
            'xyz' => [1.0217597985863, 0.9748328777982, 1.8033744871467],
            'cmy' => [0.90588235294, 0.90588235294, 0.85882352941],
            'lab' => [8.78000436678, 3.53992416085, -8.25649377318],
            'lch' => [8.7800, 8.9834, 293],
            'yxy' => [0.97483287780, 0.26888648102, 0.25653718462],
            'hlab' => [9.87336253663, 1.74865956208, -4.62672193466],
            'luv' => [8.78000436678, -0.42477811857, -5.89302652550],
        ],
        [
            'name' => '',
            'rgba' => [38, 37, 38, 1],
            'rgb' => [38, 37, 38],
            'hex' => '262526',
            'safe' => [51, 51, 51],
            'hsla' => [300, 0.01333333333, 0.14705882353, 1],
            'hlsa' => [300, 0.14705882353, 0.01333333333, 1],
            'hsl' => [300, 0.01333333333, 0.14705882353],
            'hsb' => [300, 0.02631578947, 0.14901960784],
            'cmyk' => [0.0, 0.02631578947, 0.0, 0.85098039216],
            'xyz' => [1.81074805293, 1.87514538364, 2.10022398953],
            'cmy' => [0.85098039216, 0.85490196078, 0.85098039216],
            'lab' => [14.81789498381, 0.70378561186, -0.50278419137],
            'lch' => [14.8179, 0.8649, 324],
            'yxy' => [1.87514538364, 0.31294699357, 0.32407662091],
            'hlab' => [13.69359479332, 0.37712685779, -0.2630715963],
            'luv' => [14.81789498381, 0.39098802380, -0.50788012633],
        ],
        [
            'name' => 'mintcream',
            'rgba' => [245, 255, 250, 1],
            'rgb' => [245, 255, 250],
            'hex' => 'f5fffa',
            'safe' => [255, 255, 255],
            'hsla' => [150, 1.0, 0.98039215686, 1],
            'hlsa' => [150, 0.98039215686, 1.0, 1],
            'hsl' => [150, 1.0, 0.98039215686],
            'hsb' => [150, 0.03921568627, 1],
            'cmyk' => [0.03921568627, 0, 0.01960784314, 0],
            'xyz' => [90.67150742611, 97.83460494759, 104.54754762431],
            'cmy' => [0.03921568627, 0, 0.01960784314],
            'lab' => [99.15659662606, -4.15798895661, 1.23639098886],
            'lch' => [99.1566, 4.3379, 163],
            'yxy' => [97.83460494759, 0.30940240578, 0.33384536111],
            'hlab' => [98.91137697332, -4.2484576178, 1.23106449364],
            'luv' => [99.15659662606, -5.25929892095, 2.66093387450],
        ],
        [
            'name' => 'paleturquoise',
            'rgba' => [175, 238, 238, 1],
            'rgb' => [175, 238, 238],
            'hex' => 'afeeee',
            'safe' => [153, 255, 255],
            'hsla' => [180, 0.64948453608, 0.80980392157, 1],
            'hlsa' => [180, 0.80980392157, 0.64948453608, 1],
            'hsl' => [180, 0.64948453608, 0.80980392157],
            'hsb' => [180, 0.26470588235, 0.93333333333],
            'cmyk' => [0.26470588235, 0, 0, 0.06666666667],
            'xyz' => [63.68634832352, 76.43607792171, 92.28593194951],
            'cmy' => [0.31372549020, 0.06666666667, 0.06666666667],
            'lab' => [90.06142407789, -19.63317862088, -6.40839054412],
            'lch' => [90.0614, 20.6526, 198],
            'yxy' => [76.43607792171, 0.27402778806, 0.32888695792],
            'hlab' => [87.42772896611, -18.59222675116, -6.38037588776],
            'luv' => [90.06142407789, -31.06674153612, -6.71681430275],
        ]
    ];

    public function testRGBAToRGB(): void
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->RGBAToRGB(...$color['rgba'])),
                $color['rgb']
            );
        }
    }

    protected function prepareValues($arr): array
    {
        foreach ($arr as &$item) {
            if ($item == (int)$item) {
                $item = (int)$item;
            } else {
                $item = round($item, 11);
            }
        }

        return array_values($arr);
    }

    public function testRGBToRGBA(): void
    {
        foreach ($this->colors as $color) {
            $result = $color['rgb'];
            $result[] = 1;
            self::assertEquals(
                $this->prepareValues((new Converter)->RGBToRGBA(...$color['rgb'])),
                $result
            );
        }
    }

    public function testHEXToRGB(): void
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->HEXToRGB($color['hex'])),
                $color['rgb']
            );
        }
    }

    public function testRGBToHEX(): void
    {
        foreach ($this->colors as $color) {
            $this->assertSame(
                (new Converter)->RGBToHEX(...$color['rgb']),
                $color['hex']
            );
        }
    }

    public function testCMYKToRGB(): void
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->CMYKToRGB(...$color['cmyk'])),
                $color['rgb']
            );
        }
    }

    public function testRGBToCMYK(): void
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->RGBToCMYK(...$color['rgb'])),
                $color['cmyk']
            );
        }
    }

    public function testWithExceptionNameToRGB(): void
    {
        foreach ($this->colors as $color) {
            if (!empty($color['name'])) {
                self::assertSame(
                    $this->prepareValues((new Converter)->NameToRGB(...$color['name'])),
                    $color['rgb']
                );
            } else {
                $this->expectException(InvalidArgumentException::class);
                (new Converter)->NameToRGB($color['name']);
            }
        }
    }

    public function testNameToRGB(): void
    {
        foreach ($this->colors as $color) {
            if (!empty($color['name'])) {
                self::assertSame(
                    $this->prepareValues((new Converter)->NameToRGB($color['name'])),
                    $color['rgb']
                );
            }
        }
    }

    public function testWithExceptionRGBToName(): void
    {
        foreach ($this->colors as $color) {
            if (!empty($color['name'])) {
                self::assertSame(
                    (new Converter)->RGBToName(...$color['rgb']),
                    $color['name']
                );
            } else {
                $this->expectException(InvalidArgumentException::class);
                (new Converter)->RGBToName(...$color['rgb']);
            }
        }
    }

    public function testRGBToName(): void
    {
        foreach ($this->colors as $color) {
            if (!empty($color['name'])) {
                self::assertSame(
                    (new Converter)->RGBToName(...$color['rgb']),
                    $color['name']
                );
            }
        }
    }

    public function testHSLToRGB(): void
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->HSLToRGB(...$color['hsl'])),
                $color['rgb']
            );
        }
    }

    public function testRGBToHSL(): void
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->RGBToHSL(...$color['rgb'])),
                $color['hsl']
            );
        }
    }

    public function testHSLAToRGBA(): void
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->HSLAToRGBA(...$color['hsla'])),
                $color['rgba']
            );
        }
    }

    public function testRGBAToHSLA(): void
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->RGBAToHSLA(...$color['rgba'])),
                $color['hsla']
            );
        }
    }

    public function testHSIAToRGBA(): void
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->HSIAToRGBA(...$color['hsla'])),
                $color['rgba']
            );
        }
    }

    public function testRGBAToHSIA(): void
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->RGBAToHSIA(...$color['rgba'])),
                $color['hsla']
            );
        }
    }

    public function testHLSAToRGBA(): void
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->HLSAToRGBA(...$color['hlsa'])),
                $color['rgba']
            );
        }
    }

    public function testRGBAToHLSA(): void
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->RGBAToHLSA(...$color['rgba'])),
                $color['hlsa']
            );
        }
    }

    public function testHSVToRGB(): void
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->HSVToRGB(...$color['hsb'])),
                $color['rgb']
            );
        }
    }

    public function testHSBToRGB(): void
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->HSBToRGB(...$color['hsb'])),
                $color['rgb']
            );
        }
    }

    public function testRGBToHSV(): void
    {
        foreach ($this->colors as $color) {
            if ($color['hsb'][0] === 360) {
                $actual = $color['zeroHsb'];
            } else {
                $actual = $color['hsb'];
            }
            self::assertEquals(
                $this->prepareValues((new Converter)->RGBToHSV(...$color['rgb'])),
                $actual
            );
        }
    }

    public function testRGBToHSB(): void
    {
        foreach ($this->colors as $color) {
            if ($color['hsb'][0] === 360) {
                $actual = $color['zeroHsb'];
            } else {
                $actual = $color['hsb'];
            }
            self::assertEquals(
                $this->prepareValues((new Converter)->RGBToHSB(...$color['rgb'])),
                $actual
            );
        }
    }

    public function testRGBToSafe(): void
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->RGBToSafe(...$color['rgb'])),
                $color['safe']
            );
        }
    }

    public function testRGBToXYZ()
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->RGBToXYZ(...$color['rgb'])),
                $color['xyz']
            );
        }
    }

    public function testXYZToRGB()
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->XYZToRGB(...$color['xyz'])),
                $color['rgb']
            );
        }
    }

    public function testRGBToCMY()
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->RGBToCMY(...$color['rgb'])),
                $color['cmy']
            );
        }
    }

    public function testCMYToRGB()
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->CMYToRGB(...$color['cmy'])),
                $color['rgb']
            );
        }
    }

    public function testRGBToLab()
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->RGBToLab(...$color['rgb'])),
                $color['lab']
            );
        }
    }

    public function testLabToRGB()
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->LabToRGB(...$color['lab'])),
                $color['rgb']
            );
        }
    }

    public function testRGBToLCh()
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->RGBToLCh(...$color['rgb'])),
                $color['lch']
            );
        }
    }

    public function testLChToRGB()
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->LChToRGB(...$color['lch'])),
                $color['rgb']
            );
        }
    }

    public function testRGBToYxy()
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->RGBToYxy(...$color['rgb'])),
                $color['yxy']
            );
        }
    }

    public function testYxyToRGB()
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->YxyToRGB(...$color['yxy'])),
                $color['rgb']
            );
        }
    }

    public function testRGBToHunterLab()
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->RGBToHunterLab(...$color['rgb'])),
                $color['hlab']
            );
        }
    }

    public function testHunterLabToRGB()
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->HunterLabToRGB(...$color['hlab'])),
                $color['rgb']
            );
        }
    }

    public function testRGBToLuv()
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->RGBToLuv(...$color['rgb'])),
                $color['luv']
            );
        }
    }

    public function testLuvToRGB()
    {
        foreach ($this->colors as $color) {
            self::assertEquals(
                $this->prepareValues((new Converter)->LuvToRGB(...$color['luv'])),
                $color['rgb']
            );
        }
    }

    public function testThrowsExceptionOnUnknownSchema(): void
    {
        $this->expectException(RuntimeException::class);
        (new Converter)->HelloToWorld();
    }
}
