<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes;

trait RGBToCircleTrait
{
    /**
     * Prepares RGB to HLS/HSB conversion.
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return array<int|float>
     */
    protected function RGBToCircle(int $red, int $green, int $blue): array
    {
        $r = (float)($red / 255);
        $g = (float)($green / 255);
        $b = (float)($blue / 255);

        $max = (float)max($r, $g, $b);
        $min = (float)min($r, $g, $b);
        $d = $max - $min;
        $hue = $this->getCircleHue($d, $max, $r, $g, $b);

        return compact('min', 'max', 'd', 'hue');
    }

    /**
     * Calculate circle hue.
     *
     * @param float $d
     * @param float $max
     * @param float $r
     * @param float $g
     * @param float $b
     * @return int
     */
    protected function getCircleHue(float $d, float $max, float $r, float $g, float $b): int
    {
        if ($d === 0.0) {
            return 0;
        }
        if ($max === $r) {
            $hue = fmod(($g - $b) / $d, 6);
        } elseif ($max === $g) {
            $hue = 2 + ($b - $r) / $d;
        } else {
            $hue = 4 + ($r - $g) / $d;
        }
        $hue = (int)round($hue * 60);
        if ($hue < 0) {
            $hue = 360 + $hue;
        }

        return $hue;
    }
}
