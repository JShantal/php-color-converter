<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes;

class LabScheme extends BaseLabScheme
{
    /**
     * Convert the XYZ color to a Lab color.
     *
     * @param float $x
     * @param float $y
     * @param float $z
     * @return array<float>
     */
    public function fromXYZ(float $x, float $y, float $z): array
    {
        $x /= $this->refX;
        $y /= $this->refY;
        $z /= $this->refZ;

        $x = (float)($x > 0.008856 ? $x ** (1 / 3) : (7.787 * $x) + (16 / 116));
        $y = (float)($y > 0.008856 ? $y ** (1 / 3) : (7.787 * $y) + (16 / 116));
        $z = (float)($z > 0.008856 ? $z ** (1 / 3) : (7.787 * $z) + (16 / 116));

        $l = (116 * $y) - 16;
        $a = 500 * ($x - $y);
        $b = 200 * ($y - $z);

        return ['l' => (float)$l, 'a' => (float)$a, 'b' => (float)$b];
    }

    /**
     * Convert the Lab color to a XYZ color.
     *
     * @param float $l
     * @param float $a
     * @param float $b
     * @return array<float>
     */
    public function toXYZ(float $l, float $a, float $b): array
    {
        $y = ($l + 16) / 116;
        $x = $a / 500 + $y;
        $z = $y - $b / 200;

        $y = ($y ** 3) > 0.008856 ? $y ** 3 : ($y - 16 / 116) / 7.787;
        $x = ($x ** 3) > 0.008856 ? $x ** 3 : ($x - 16 / 116) / 7.787;
        $z = ($z ** 3) > 0.008856 ? $z ** 3 : ($z - 16 / 116) / 7.787;

        $x *= $this->refX;
        $y *= $this->refY;
        $z *= $this->refZ;

        return compact('x', 'y', 'z');
    }
}
