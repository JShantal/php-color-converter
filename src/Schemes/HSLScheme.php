<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes;

class HSLScheme extends AbstractScheme
{
    use RGBToCircleTrait;

    /**
     * Convert the RGB color to a HSL color.
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return array<int|float>
     */
    public function fromRGB(int $red, int $green, int $blue): array
    {
        ['min' => $min, 'max' => $max, 'd' => $d, 'hue' => $hue] = $this->RGBToCircle($red, $green, $blue);
        $lightness = ($max + $min) / 2;
        $saturation = ($d === 0.0) ? 0 : ($d / (1 - abs(2 * $lightness - 1)));
        $saturation = (float)$saturation;

        return compact('hue', 'saturation', 'lightness');
    }

    /**
     * Convert the HSL color to a RGB color.
     *
     * @param int $hue
     * @param float $saturation
     * @param float $lightness
     * @return array<int>
     */
    public function toRGB(int $hue, float $saturation, float $lightness): array
    {
        if ($saturation === 0.0) {
            $lightness *= 255;

            return $this->roundRGB($lightness, $lightness, $lightness);
        }

        $h = (float)($hue / 360);
        $v2 = ($lightness < 0.5)
            ? ($lightness * (1 + $saturation))
            : (($lightness + $saturation) - ($lightness * $saturation));
        $v1 = 2 * $lightness - $v2;

        $red = 255 * $this->hueToRGB($v1, $v2, $h + (1 / 3));
        $green = 255 * $this->hueToRGB($v1, $v2, $h);
        $blue = 255 * $this->hueToRGB($v1, $v2, $h - (1 / 3));

        return $this->roundRGB($red, $green, $blue);
    }

    /**
     * Prepare Hue value to RGB conversion.
     *
     * @param float $v1
     * @param float $v2
     * @param float $vH
     * @return float
     */
    protected function hueToRGB(float $v1, float $v2, float $vH): float
    {
        if ($vH < 0) {
            ++$vH;
        }
        if ($vH > 1) {
            --$vH;
        }
        if ((6 * $vH) < 1) {
            return ($v1 + ($v2 - $v1) * 6 * $vH);
        }
        if ((2 * $vH) < 1) {
            return $v2;
        }
        if ((3 * $vH) < 2) {
            return ($v1 + ($v2 - $v1) * ((2.0 / 3) - $vH) * 6);
        }

        return $v1;
    }
}
