<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes;

class HLSScheme extends AbstractScheme
{
    /**
     * @var HSLScheme
     */
    protected $HSLScheme;

    /**
     * HLS Scheme constructor.
     */
    public function __construct()
    {
        $this->HSLScheme = new HSLScheme();
    }

    /**
     * Convert the RGB color to a HLS color.
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return array<int|float>
     */
    public function fromRGB(int $red, int $green, int $blue): array
    {
        $hsl = $this->HSLScheme->fromRGB($red, $green, $blue);
        ksort($hsl);

        return $hsl;
    }

    /**
     * Convert the HLS color to a RGB color.
     *
     * @param int $hue
     * @param float $lightness
     * @param float $saturation
     * @return array<int>
     */
    public function toRGB(int $hue, float $lightness, float $saturation): array
    {
        return $this->HSLScheme->toRGB($hue, $saturation, $lightness);
    }
}
