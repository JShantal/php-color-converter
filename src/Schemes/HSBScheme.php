<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes;

class HSBScheme extends AbstractScheme
{
    /**
     * @var HSVScheme
     */
    protected $HSVScheme;

    public function __construct()
    {
        $this->HSVScheme = new HSVScheme();
    }

    /**
     * Convert the HSB color to a RGB color.
     *
     * @param int $hue
     * @param float $saturation
     * @param float $brightness
     * @return array<int>
     */
    public function toRGB(int $hue, float $saturation, float $brightness): array
    {
        return $this->HSVScheme->toRGB($hue, $saturation, $brightness);
    }

    /**
     * Convert the RGB color to a HSB color.
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return array<int|float>
     */
    public function fromRGB(int $red, int $green, int $blue): array
    {
        return $this->HSVScheme->fromRGB($red, $green, $blue);
    }
}
