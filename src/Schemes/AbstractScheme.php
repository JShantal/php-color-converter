<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes;

abstract class AbstractScheme
{
    use RoundRGBTrait;
}
