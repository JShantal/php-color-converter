<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes;

use BoomDraw\ColorConverter\Schemes\XYZ\RGBOverXYZTrait;

class YxyScheme extends AbstractScheme
{
    use RGBOverXYZTrait;

    /**
     * Convert the XYZ color to a Yxy color.
     *
     * @param float $X
     * @param float $Y
     * @param float $Z
     * @return array<float>
     */
    public function fromXYZ(float $X, float $Y, float $Z): array
    {
        $x = $X / ($X + $Y + $Z);
        $y = $Y / ($X + $Y + $Z);

        return compact('Y', 'x', 'y');
    }

    /**
     * Convert the Yxy color to a XYZ color.
     *
     * @param float $Y
     * @param float $x
     * @param float $y
     * @return array<int>
     */
    public function toXYZ(float $Y, float $x, float $y): array
    {
        $X = $x * ($Y / $y);
        $Z = (1 - $x - $y) * ($Y / $y);

        return ['x' => $X, 'y' => $Y, 'z' => $Z];
    }
}
