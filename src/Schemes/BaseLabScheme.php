<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes;

use BoomDraw\ColorConverter\Schemes\XYZ\RGBOverXYZTrait;

abstract class BaseLabScheme extends AbstractScheme
{
    use RGBOverXYZTrait;

    /**
     * @var float
     */
    protected $refX = 95.047;

    /**
     * @var float
     */
    protected $refY = 100.0;

    /**
     * @var float
     */
    protected $refZ = 108.883;

    /**
     * Lab Scheme constructor.
     *
     * @param float $refX
     * @param float $refY
     * @param float $refZ
     */
    public function __construct(float $refX = 95.047, float $refY = 100.0, float $refZ = 108.883)
    {
        $this->setRef($refX, $refY, $refZ);
    }

    /**
     * Set scheme ref values.
     *
     * @param float $x
     * @param float $y
     * @param float $z
     * @return $this
     */
    public function setRef(float $x, float $y, float $z): self
    {
        $this->setRefX($x);
        $this->setRefY($y);
        $this->setRefZ($z);

        return $this;
    }

    /**
     * Set scheme ref x value.
     *
     * @param float $x
     * @return $this
     */
    public function setRefX(float $x): self
    {
        $this->refX = $x;

        return $this;
    }

    /**
     * Set scheme ref y value.
     *
     * @param float $y
     * @return $this
     */
    public function setRefY(float $y): self
    {
        $this->refY = $y;

        return $this;
    }

    /**
     * Set scheme ref z value.
     *
     * @param float $z
     * @return $this
     */
    public function setRefZ(float $z): self
    {
        $this->refZ = $z;

        return $this;
    }
}
