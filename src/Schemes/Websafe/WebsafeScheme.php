<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes\Websafe;

use BoomDraw\ColorConverter\Schemes\AbstractScheme;

class WebsafeScheme extends AbstractScheme
{
    /**
     * @var array<array<int>>
     */
    protected $websafeColors;

    /**
     * Websafe Scheme constructor.
     */
    public function __construct()
    {
        $this->websafeColors = include('websafe_colors.php');
    }

    /**
     * Convert the RGB color to a RGB color from the safe color list.
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return array<int>
     */
    public function fromRGB(int $red, int $green, int $blue): array
    {
        $colors = $this->websafeColors;
        $nearest = array_shift($colors);
        $shortest_distance = $this->getRGBDistance($red, $green, $blue, ...$nearest);
        foreach ($colors as $color) {
            $current_distance = $this->getRGBDistance($red, $green, $blue, ...$color);
            if ($current_distance < $shortest_distance) {
                $shortest_distance = $current_distance;
                $nearest = $color;
            }
        }

        return $this->roundRGB(...$nearest);
    }

    /**
     * Return distance between two RGB colors.
     *
     * @param int $r0
     * @param int $g0
     * @param int $b0
     * @param int $r1
     * @param int $g1
     * @param int $b1
     * @return int
     */
    protected function getRGBDistance(int $r0, int $g0, int $b0, int $r1, int $g1, int $b1): int
    {
        $diff = abs($r0 - $r1);
        $diff += abs($g0 - $g1);
        $diff += abs($b0 - $b1);

        return $diff;
    }

    /**
     * Return the same RGB color to prevent convertor error.
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return array<int>
     */
    public function toRGB(int $red, int $green, int $blue): array
    {
        return $this->roundRGB($red, $green, $blue);
    }
}
