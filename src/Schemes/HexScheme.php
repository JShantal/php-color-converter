<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes;

class HexScheme extends AbstractScheme
{
    /**
     * Convert the HEX color to a RGB color.
     *
     * @param string $hex
     * @return array<int>
     */
    public function toRGB(string $hex): array
    {
        $rgb = sscanf($hex, "%02x%02x%02x");

        return $this->roundRGB(...$rgb);
    }

    /**
     * Convert the RGB color to a HEX color
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return string
     */
    public function fromRGB(int $red, int $green, int $blue): string
    {
        return sprintf("%02x%02x%02x", $red, $green, $blue);
    }
}
