# PHP color converter

[![Build Status](https://img.shields.io/scrutinizer/build/g/boomdraw/color-converter.svg?style=flat-square)](https://scrutinizer-ci.com/g/boomdraw/color-converter)
[![StyleCI](https://github.styleci.io/repos/282461567/shield?branch=master)](https://github.styleci.io/repos/282461567)
[![Code Coverage](https://img.shields.io/scrutinizer/coverage/g/boomdraw/color-converter.svg?style=flat-square)](https://scrutinizer-ci.com/g/boomdraw/color-converter)
[![Quality Score](https://img.shields.io/scrutinizer/g/boomdraw/color-converter.svg?style=flat-square)](https://scrutinizer-ci.com/g/boomdraw/color-converter)
[![Latest Version on Packagist](https://img.shields.io/packagist/v/boomdraw/color-converter?style=flat-square)](https://packagist.org/packages/boomdraw/color-converter)
[![Total Downloads](https://img.shields.io/packagist/dt/boomdraw/color-converter.svg?style=flat-square)](https://packagist.org/packages/boomdraw/color-converter)
[![PHP Version](https://img.shields.io/packagist/php-v/boomdraw/color-converter?style=flat-square)](https://packagist.org/packages/boomdraw/color-converter)
[![License](https://img.shields.io/packagist/l/boomdraw/color-converter?style=flat-square?style=flat-square)](https://packagist.org/packages/boomdraw/color-converter)

This package allows converting color between RGB, RGBA, HEX, CMYK, HSL, HSLA, HSI, HSIA, HLS, HLSA, HSV, HSB, html name, and websafe rgb schemas.

## Installation

Via Composer

```bash
$ composer require boomdraw/color-converter
```

## Usage

## Examples

## Testing

### Package testing

You can run the tests with:

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details and a todo list.

## Security

If you discover any security-related issues, please email [pkgsecurity@boomdraw.com](mailto:pkgsecurity@boomdraw.com) instead of using the issue tracker.

## License

[MIT](LICENSE)
