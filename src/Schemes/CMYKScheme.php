<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes;

class CMYKScheme extends AbstractScheme
{
    /**
     * Convert the CMYK color to a RGB color.
     *
     * @param float $cyan
     * @param float $magenta
     * @param float $yellow
     * @param float $key
     * @return array<int>
     */
    public function toRGB(float $cyan, float $magenta, float $yellow, float $key): array
    {
        $key = (1 - $key) * 255;
        $red = (1 - $cyan) * $key;
        $green = (1 - $magenta) * $key;
        $blue = (1 - $yellow) * $key;

        return $this->roundRGB($red, $green, $blue);
    }

    /**
     * Convert the RGB color to a CMYK color.
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return array<float>
     */
    public function fromRGB(int $red, int $green, int $blue): array
    {
        $r = (float)($red / 255);
        $g = (float)($green / 255);
        $b = (float)($blue / 255);

        $key = (float)(1 - max($r, $g, $b));
        $cyan = (float)abs((1 - $r / (1 - $key)));
        $magenta = (float)abs((1 - $g / (1 - $key)));
        $yellow = (float)abs((1 - $b / (1 - $key)));

        return compact('cyan', 'magenta', 'yellow', 'key');
    }
}
