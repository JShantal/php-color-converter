<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes\Name;

use BoomDraw\ColorConverter\Schemes\AbstractScheme;
use InvalidArgumentException;

class NameScheme extends AbstractScheme
{
    /**
     * @var array<array<int>>
     */
    protected $colorNames;

    public function __construct()
    {
        $this->colorNames = include('html_color_names.php');
    }

    /**
     * Convert the HTML named color to a RGB color.
     *
     * @param string $name
     * @return array<int>
     */
    public function toRGB(string $name): array
    {
        if (!array_key_exists($name, $this->colorNames)) {
            throw new InvalidArgumentException('Color name not found!');
        }

        return $this->roundRGB(...$this->colorNames[$name]);
    }

    /**
     * Convert the RGB color to a HTML named color.
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return string
     */
    public function fromRGB(int $red, int $green, int $blue): string
    {
        $color = array_search([$red, $green, $blue], $this->colorNames, true);

        if ($color === false) {
            throw new InvalidArgumentException('Name for color not found!');
        }

        return $color;
    }
}
