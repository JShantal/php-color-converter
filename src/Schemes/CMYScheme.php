<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes;

class CMYScheme extends AbstractScheme
{
    /**
     * Convert the RGB color to a CMY color.
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return array<float>
     */
    public function fromRGB(int $red, int $green, int $blue): array
    {
        $cyan = (1 - ($red / 255));
        $magenta = (1 - ($green / 255));
        $yellow = (1 - ($blue / 255));

        return compact('cyan', 'magenta', 'yellow');
    }

    /**
     * Convert the CMY color to a RGB color.
     *
     * @param float $cyan
     * @param float $magenta
     * @param float $yellow
     * @return array<int>
     */
    public function toRGB(float $cyan, float $magenta, float $yellow): array
    {
        $red = (1 - $cyan) * 255;
        $green = (1 - $magenta) * 255;
        $blue = (1 - $yellow) * 255;

        return $this->roundRGB($red, $green, $blue);
    }
}
