<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes;

class HunterLabScheme extends BaseLabScheme
{
    /**
     * Convert the XYZ color to a HunterLab color.
     *
     * @param float $x
     * @param float $y
     * @param float $z
     * @return array<float>
     */
    public function fromXYZ(float $x, float $y, float $z): array
    {
        $kA = (175 / 198.04) * ($this->refY + $this->refX);
        $kB = (70 / 218.11) * ($this->refY + $this->refZ);

        $l = 100 * sqrt($y / $this->refY);
        $a = $kA * ((($x / $this->refX) - ($y / $this->refY)) / sqrt($y / $this->refY));
        $b = $kB * ((($y / $this->refY) - ($z / $this->refZ)) / sqrt($y / $this->refY));

        return ['l' => (float)$l, 'a' => (float)$a, 'b' => (float)$b];
    }

    /**
     * Convert the HunterLab color to a XYZ color.
     *
     * @param float $l
     * @param float $a
     * @param float $b
     * @return array<int>
     */
    public function toXYZ(float $l, float $a, float $b): array
    {
        $Ka = (175.0 / 198.04) * ($this->refY + $this->refX);
        $Kb = (70.0 / 218.11) * ($this->refY + $this->refZ);

        $y = (($l / $this->refY) ** 2) * 100;
        $x = ($a / $Ka * sqrt($y / $this->refY) + ($y / $this->refY)) * $this->refX;
        $z = -($b / $Kb * sqrt($y / $this->refY) - ($y / $this->refY)) * $this->refZ;

        return compact('x', 'y', 'z');
    }
}
