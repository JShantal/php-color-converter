# Changelog

All notable changes to `php-color-converter` will be documented in this file

## 1.0.0 - 2021-01-10

- Initial release
