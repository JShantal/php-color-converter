<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes;

trait RoundRGBTrait
{
    /**
     * Rounds the RGB colors to integers.
     *
     * @param int|float $red
     * @param int|float $green
     * @param int|float $blue
     * @return array<int>
     */
    protected function roundRGB($red, $green, $blue): array
    {
        $red = (int)round($red);
        $green = (int)round($green);
        $blue = (int)round($blue);

        return compact('red', 'green', 'blue');
    }
}
