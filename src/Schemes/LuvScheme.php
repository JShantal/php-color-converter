<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes;

class LuvScheme extends BaseLabScheme
{
    /**
     * Convert the XYZ color to a Luv color.
     *
     * @param float $x
     * @param float $y
     * @param float $z
     * @return array<float>
     */
    public function fromXYZ(float $x, float $y, float $z): array
    {
        $u = (4 * $x) / ($x + (15 * $y) + (3 * $z));
        $v = (9 * $y) / ($x + (15 * $y) + (3 * $z));
        $y /= 100;
        $y = (float)($y > 0.008856 ? $y ** (1 / 3) : (7.787 * $y) + (16 / 116));

        $l = (116 * $y) - 16;
        $u = 13 * $l * ($u - $this->getRefU());
        $v = 13 * $l * ($v - $this->getRefV());

        return ['l' => (float)$l, 'u' => (float)$u, 'v' => (float)$v];
    }

    /**
     * Calculate ref u.
     *
     * @return float
     */
    protected function getRefU(): float
    {
        return (4 * $this->refX) / ($this->refX + (15 * $this->refY) + (3 * $this->refZ));
    }

    /**
     * Calculate ref v.
     *
     * @return float
     */
    protected function getRefV(): float
    {
        return (9 * $this->refY) / ($this->refX + (15 * $this->refY) + (3 * $this->refZ));
    }

    /**
     * Convert the Luv color to a XYZ color.
     *
     * @param float $l
     * @param float $u
     * @param float $v
     * @return array<float>
     */
    public function toXYZ(float $l, float $u, float $v): array
    {
        $y = ($l + 16) / 116;
        $y = ($y ** 3) > 0.008856 ? $y ** 3 : (($y - 16 / 116) / 7.787);

        $u = $u / (13 * $l) + $this->getRefU();
        $v = $v / (13 * $l) + $this->getRefV();

        $y *= 100;
        $x = -(9 * $y * $u) / (($u - 4) * $v - $u * $v);
        $z = (9 * $y - (15 * $v * $y) - ($v * $x)) / (3 * $v);

        return compact('x', 'y', 'z');
    }
}
