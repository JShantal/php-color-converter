<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes;

class HSIScheme extends AbstractScheme
{
    /**
     * @var HSLScheme
     */
    protected $HSLScheme;

    /**
     * HLS Scheme constructor.
     */
    public function __construct()
    {
        $this->HSLScheme = new HSLScheme();
    }

    /**
     * Convert the RGB color to a HSI color.
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return array<int|float>
     */
    public function fromRGB(int $red, int $green, int $blue): array
    {
        return $this->HSLScheme->fromRGB($red, $green, $blue);
    }

    /**
     * Convert the HSI color to a RGB color.
     *
     * @param int $hue
     * @param float $saturation
     * @param float $intensity
     * @return array<int>
     */
    public function toRGB(int $hue, float $saturation, float $intensity): array
    {
        return $this->HSLScheme->toRGB($hue, $saturation, $intensity);
    }
}
