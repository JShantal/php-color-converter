<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes\XYZ;

use BoomDraw\ColorConverter\Schemes\AbstractScheme;

class XYZScheme extends AbstractScheme
{
    /**
     * Convert the RGB color to a XYZ color.
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return array<float>
     */
    public function fromRGB(int $red, int $green, int $blue): array
    {
        $r = $this->prepareColorToXYZConversion($red);
        $g = $this->prepareColorToXYZConversion($green);
        $b = $this->prepareColorToXYZConversion($blue);

        $x = $r * 0.4124 + $g * 0.3576 + $b * 0.1805;
        $y = $r * 0.2126 + $g * 0.7152 + $b * 0.0722;
        $z = $r * 0.0193 + $g * 0.1192 + $b * 0.9505;

        return compact('x', 'y', 'z');
    }

    /**
     * Prepare color to XYZ conversion.
     *
     * @param int $color
     * @return float
     */
    protected function prepareColorToXYZConversion(int $color): float
    {
        $floatColor = $color / 255;
        $floatColor = $floatColor > 0.04045 ? (($floatColor + 0.055) / 1.055) ** 2.4 : $floatColor / 12.92;

        return $floatColor * 100;
    }

    /**
     * Convert the XYZ color to a RGB color.
     *
     * @param float $x
     * @param float $y
     * @param float $z
     * @return array<int>
     */
    public function toRGB(float $x, float $y, float $z): array
    {
        $x /= 100;
        $y /= 100;
        $z /= 100;

        $red = $x * 3.2406 + $y * -1.5372 + $z * -0.4986;
        $green = $x * -0.9689 + $y * 1.8758 + $z * 0.0415;
        $blue = $x * 0.0557 + $y * -0.2040 + $z * 1.0570;

        $red = $this->prepareColorForXYZToRGBOutput((float)$red);
        $green = $this->prepareColorForXYZToRGBOutput((float)$green);
        $blue = $this->prepareColorForXYZToRGBOutput((float)$blue);

        return $this->roundRGB($red, $green, $blue);
    }

    /**
     * Prepare color for XYZ to RGB output.
     *
     * @param float $color
     * @return float
     */
    protected function prepareColorForXYZToRGBOutput(float $color): float
    {
        $color = ($color > 0.0031308) ? (1.055 * ($color ** (1 / 2.4)) - 0.055) : (12.92 * $color);

        return $color * 255;
    }
}
