<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes;

class RGBAScheme extends AbstractScheme
{
    /**
     * Convert the RGBA color to a RGB color.
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @param float $alpha
     * @param int $bgRed
     * @param int $bgGreen
     * @param int $bgBlue
     * @return array<int>
     */
    public function toRGB(
        int $red,
        int $green,
        int $blue,
        float $alpha,
        int $bgRed = 255,
        int $bgGreen = 255,
        int $bgBlue = 255
    ): array {
        $r = ((1 - $alpha) * $bgRed) + ($alpha * $red);
        $g = ((1 - $alpha) * $bgGreen) + ($alpha * $green);
        $b = ((1 - $alpha) * $bgBlue) + ($alpha * $blue);

        return $this->roundRGB($r, $g, $b);
    }

    /**
     * Convert the RGB color to a RGBA color.
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return array<int>
     */
    public function fromRGB(int $red, int $green, int $blue): array
    {
        $alpha = 1;

        return compact('red', 'green', 'blue', 'alpha');
    }
}
