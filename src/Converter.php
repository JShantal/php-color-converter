<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter;

use BoomDraw\ColorConverter\Schemes\CMYKScheme;
use BoomDraw\ColorConverter\Schemes\CMYScheme;
use BoomDraw\ColorConverter\Schemes\HexScheme;
use BoomDraw\ColorConverter\Schemes\HLSScheme;
use BoomDraw\ColorConverter\Schemes\HSBScheme;
use BoomDraw\ColorConverter\Schemes\HSIScheme;
use BoomDraw\ColorConverter\Schemes\HSLScheme;
use BoomDraw\ColorConverter\Schemes\HSVScheme;
use BoomDraw\ColorConverter\Schemes\HunterLabScheme;
use BoomDraw\ColorConverter\Schemes\LabScheme;
use BoomDraw\ColorConverter\Schemes\LChScheme;
use BoomDraw\ColorConverter\Schemes\LuvScheme;
use BoomDraw\ColorConverter\Schemes\Name\NameScheme;
use BoomDraw\ColorConverter\Schemes\RGBAScheme;
use BoomDraw\ColorConverter\Schemes\RoundRGBTrait;
use BoomDraw\ColorConverter\Schemes\Websafe\WebsafeScheme;
use BoomDraw\ColorConverter\Schemes\XYZ\XYZScheme;
use BoomDraw\ColorConverter\Schemes\YxyScheme;
use Closure;
use RuntimeException;

class Converter
{
    use RoundRGBTrait;

    protected const ALPHA_SCHEMES = [
        'rgba',
        'hsia',
        'hsla',
        'hlsa',
    ];

    /**
     * Color converter schemes.
     *
     * @var array<string>
     */
    protected $schemes = [
        'cmyk' => CMYKScheme::class,
        'cmy' => CMYScheme::class,
        'hex' => HexScheme::class,
        'hls' => HLSScheme::class,
        'hsb' => HSBScheme::class,
        'hsi' => HSIScheme::class,
        'hsl' => HSLScheme::class,
        'hsv' => HSVScheme::class,
        'hunterlab' => HunterLabScheme::class,
        'lab' => LabScheme::class,
        'lch' => LChScheme::class,
        'luv' => LuvScheme::class,
        'name' => NameScheme::class,
        'rgba' => RGBAScheme::class,
        'safe' => WebsafeScheme::class,
        'websafe' => WebsafeScheme::class,
        'xyz' => XYZScheme::class,
        'yxy' => YxyScheme::class,
    ];

    /**
     * Converter constructor.
     *
     * @param array $schemes
     */
    public function __construct(array $schemes = [])
    {
        $this->schemes = array_merge($this->schemes, $schemes);
    }

    /**
     * @param string $name
     * @param mixed[] $arguments
     * @return mixed
     */
    public function __call(string $name, array $arguments = [])
    {
        [$from, $to] = explode('To', $name, 2);

        return $this->convert($from, $to, $arguments);
    }

    /**
     * @param string $from
     * @param string $to
     * @param mixed[] $arguments
     * @return mixed
     */
    public function convert(string $from, string $to, array $arguments = [])
    {
        $fromAlpha = $this->isAlpha($from);
        $toAlpha = $this->isAlpha($to);
        if ($fromAlpha && $toAlpha) {
            return $this->convertAlpha($from, $to, $arguments);
        }
        if ($fromAlpha) {
            $rgb = $this->convertAlpha($from, 'rgba', $arguments);

            return $this->convertScheme('rgba', $to, $rgb);
        }
        if ($toAlpha) {
            $rgb = $this->convertScheme($from, 'rgb', $arguments);
            /** @var array<int> $rgba */
            $rgba = $this->convertScheme('rgb', 'rgba', $rgb);

            return $this->convertAlpha('rgba', $to, $rgba);
        }

        return $this->convertScheme($from, $to, $arguments);
    }

    /**
     * Determine the scheme is alpha.
     *
     * @param string $scheme
     * @return bool
     */
    protected function isAlpha(string $scheme): bool
    {
        $scheme = strtolower($scheme);

        return in_array($scheme, self::ALPHA_SCHEMES);
    }

    /**
     * @param string $from
     * @param string $to
     * @param mixed[] $arguments
     * @return mixed[]
     */
    protected function convertAlpha(string $from, string $to, array $arguments): array
    {
        $arguments = array_values($arguments);
        $alpha = array_pop($arguments);
        $from = $this->getBaseSchemeFromAlpha($from);
        $to = $this->getBaseSchemeFromAlpha($to);
        $result = $this->convertScheme($from, $to, $arguments);
        $result['alpha'] = $alpha;

        return $result;
    }

    /**
     * @param string $scheme
     * @return string
     */
    protected function getBaseSchemeFromAlpha(string $scheme): string
    {
        return substr($scheme, 0, -1);
    }

    /**
     * @param string $from
     * @param string $to
     * @param mixed $arguments
     * @return mixed
     */
    protected function convertScheme(string $from, string $to, array $arguments)
    {
        $arguments = array_values($arguments);
        $rgb = $this->toRgb($from, $arguments);

        return $this->fromRGB($to, $rgb);
    }

    /**
     * @param string $from
     * @param mixed $arguments
     * @return array<int>
     */
    protected function toRgb(string $from, array $arguments): array
    {
        $from = strtolower($from);
        if ($from === 'rgb') {
            return $this->roundRGB(...$arguments);
        }

        return $this->getSchemeClass($from)->toRGB(...$arguments);
    }

    /**
     * Return color scheme class by scheme name.
     *
     * @param string $name
     * @return mixed
     */
    protected function getSchemeClass(string $name)
    {
        $class = $this->schemes[$name] ?? null;
        if (!$class) {
            throw new RuntimeException("Unable to find $name scheme!");
        }
        if ($class instanceof Closure) {
            return $class();
        }
        if (!class_exists($class)) {
            throw new RuntimeException("Unable to find $name scheme class!");
        }
        return new $class;
    }

    /**
     * Convert the RGB color to a scheme color.
     *
     * @param string $to
     * @param array $rgb
     * @return string|array<float>|array<int>|array<int|float>
     */
    protected function fromRGB(string $to, array $rgb)
    {
        $to = strtolower($to);
        if ($to === 'rgb') {
            return $rgb;
        }
        $rgb = array_values($rgb);

        return $this->getSchemeClass($to)->fromRGB(...$rgb);
    }

}
