<?php

declare(strict_types=1);

namespace BoomDraw\ColorConverter\Schemes;

class HSVScheme extends AbstractScheme
{
    use RGBToCircleTrait;

    /**
     * Convert the RGB color to a HSV color.
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return array<int|float>
     */
    public function fromRGB(int $red, int $green, int $blue): array
    {
        ['max' => $max, 'd' => $d, 'hue' => $hue] = $this->RGBToCircle($red, $green, $blue);
        $value = $max;
        $saturation = ($max === 0.0) ? 0 : ($d / $max);
        $saturation = (float)$saturation;

        return compact('hue', 'saturation', 'value');
    }

    /**
     * Convert the HSV color to a RGB color.
     *
     * @param int $hue
     * @param float $saturation
     * @param float $value
     * @return array<int>
     */
    public function toRGB(int $hue, float $saturation, float $value): array
    {
        $value *= 255;
        if ($saturation === 0.0) {
            return $this->roundRGB($value, $value, $value);
        }

        $tone = $hue === 360 ? 0 : $hue / 60;
        $i = (int)floor($tone);
        $f = $tone - $i;

        $p = $value * (1 - $saturation);
        $q = $value * (1 - ($saturation * $f));
        $t = $value * (1 - ($saturation * (1 - $f)));

        $rgb = $this->getRGBByTone($i, $value, $p, $q, $t);

        return $this->roundRGB(...$rgb);
    }

    /**
     * Determine RGB values by tone.
     *
     * @param int $tone
     * @param float $value
     * @param float $p
     * @param float $q
     * @param float $t
     * @return float[]
     */
    protected function getRGBByTone(int $tone, float $value, float $p, float $q, float $t): array
    {
        switch ($tone) {
            case 0:
                return [$value, $t, $p];
            case 1:
                return [$q, $value, $p];
            case 2:
                return [$p, $value, $t];
            case 3:
                return [$p, $q, $value];
            case 4:
                return [$t, $p, $value];
            default:
                return [$value, $p, $q];
        }
    }
}
